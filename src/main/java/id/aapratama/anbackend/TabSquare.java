package id.aapratama.anbackend;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api")
public class TabSquare {

    @GetMapping(value = "address", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<TabSquareResponse> getAddress() {
        TabSquareResponse tabSquareResponse = new TabSquareResponse();
        tabSquareResponse.setResult(true);

        ArrayList<TabSquareModel> tabSquareModels = new ArrayList<TabSquareModel>();
        TabSquareModel model = new TabSquareModel();
        model.setAddress("Singapore 822323");
        model.setCountry("Singapore");

        ArrayList<TabSquareCoordinate> coordinates = new ArrayList<TabSquareCoordinate>();
        for (int i = 0; i < 2; i++) {
            TabSquareCoordinate coordinate = new TabSquareCoordinate();
            coordinate.setLat(1.4121680000000001);
            coordinate.setLng(103.897809);
            coordinates.add(coordinate);
        }

        model.setCoordinate(coordinates);
        tabSquareModels.add(model);

        tabSquareResponse.setData(tabSquareModels);

        return ResponseEntity.ok(tabSquareResponse);
    }

}
