package id.aapratama.anbackend;

import java.util.List;

public class TabSquareModel {
    private String address;
    private String country;
    private List<TabSquareCoordinate> coordinate;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<TabSquareCoordinate> getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(List<TabSquareCoordinate> coordinate) {
        this.coordinate = coordinate;
    }
}
