package id.aapratama.anbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnbackendApplication.class, args);
	}

}
