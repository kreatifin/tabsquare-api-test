package id.aapratama.anbackend;

import java.util.List;

public class TabSquareResponse {
    private Boolean result;
    private List<TabSquareModel> data;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public List<TabSquareModel> getData() {
        return data;
    }

    public void setData(List<TabSquareModel> data) {
        this.data = data;
    }
}
